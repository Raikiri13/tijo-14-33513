package pl.edu.pwsztar.domain.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PeselValidator {

    public static boolean isValid(final String login) {
        final int LOGIN_MIN_LENGTH = 4;

        if(login == null || login.length() < LOGIN_MIN_LENGTH) {
            return false;
        }

        String regex = "([0-9]){11}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(login);

        if ( matcher.matches() ){
            login.replace("\n","");
            String[] peselTable = login.split("");
            int[] weights = {1,3,7,9,1,3,7,9,1,3};
            int generator = 0;
            for( int i = 0; i < peselTable.length-1; i++ ){
                generator += Integer.parseInt(peselTable[i])*weights[i];
            }
            generator = 10-(generator%10);
            if( generator == Integer.parseInt(peselTable[10]) ){
                return true;
            } else {
                return false;
            }
        }

        return matcher.matches();
    }

}
